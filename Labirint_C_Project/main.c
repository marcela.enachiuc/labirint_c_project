#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#define N 100
#define M 100
#include "TDA.h"

int main()
{
    int i,j,n,m;
    char a[N][M],q;

    meniu();//meniul

    FILE *f=fopen("date.in","r");//citire matrice fisier
    fscanf(f,"%d",&n);
    fscanf(f,"%d",&m);

    for(i=0; i<=n; i++)
    {
        for(j=0; j<=m; j++)
        {
            fflush(stdin);
            fscanf(f,"%c",&a[i][j]);
        }
    }

    do// COMENZI MENIU
    {
        q=getch();
        switch(q)

        {
        case 'r'://reguli

            reguli();

            break;

        case 'z'://start//jucatorul ajunge in pozitia 0,pozitia de start

            system("CLS");

            struct evidenta H = {0,0};

            struct coordonate L = {1,2};

            a[L.x][L.y]='P';

            print(m, n, a);

            break;

        case 'w'://sus//asta ar trebui sa mute persoana cu 20 patratele inapoi deci ajunge sus

            system("CLS");

            if(a[L.x-1][L.y]!='X')//sus deci linia scade cu 1 => x-1
            {
                a[L.x][L.y]='_';
                L.x=L.x-1;
                if(a[L.x][L.y]=='*')
                {
                    H.v++;
                    printf("POINTS %d/%d",H.v,10);
                }
                if(a[L.x][L.y]=='H')
                {
                    H.V++;
                    printf("LIFE %d/%d   ",3-H.V,3);
                }
                a[L.x][L.y]='P';
            }
            else
                printf("Nu se poate realiza actiunea");

            print(m, n, a);

            break;

        case 's'://jos//20 de patratele inainte ca sa ajumga dedesupt deci linia x creste cu 1 coloana ramane aceeasi

            system("CLS");

            if(a[L.x+1][L.y]!='X')
            {
                a[L.x][L.y]='_';
                L.x=L.x+1;
                if(a[L.x][L.y]=='*')
                {
                    H.v++;    //numara punctele
                    printf("POINTS %d/%d",H.v,10);
                }
                if(a[L.x][L.y]=='H')
                {
                    H.V++;    //afiseaza vietile ramase
                    printf("LIFE %d/%d   ",3-H.V,3);
                }
                a[L.x][L.y]='P';
            }
            else
                printf("Nu se poate realiza actiunea");

            print(m, n, a);

            break;

        case 'a'://stanga//o patratica deci se modifica coloana cu -1

            system("CLS");

            if(a[L.x][L.y-1]!='X')
            {
                a[L.x][L.y]='_';
                L.y=L.y-1;
                if(a[L.x][L.y]=='*')
                {
                    H.v++;
                    printf("POINTS %d/%d",H.v,10);
                }
                if(a[L.x][L.y]=='H')
                {
                    H.V++;
                    printf("LIFE %d/%d   ",3-H.V,3);
                }
                a[L.x][L.y]='P';
            }
            else
                printf("Nu se poate realiza actiunea");

            print(m, n, a);

            break;

        case 'd'://dreapta//coloana creste cu +1

            system("CLS");

            if(a[L.x][L.y+1]!='X')
            {
                a[L.x][L.y]='_';
                L.y=L.y+1;
                if(a[L.x][L.y]=='*')
                {
                    H.v++;
                    printf("POINTS %d/%d",H.v,10);
                }
                if(a[L.x][L.y]=='H')
                {
                    H.V++;
                    printf("LIFE %d/%d   ",3-H.V,3);
                }
                a[L.x][L.y]='P';
            }
            else
                printf(" Nu se poate realiza actiunea");

            print(m, n, a);

            if(H.v==10)
            {
                win();
            }
            if(H.V==3)
            {
                dead();
            }
            break;
        }
    }
    while(q!='m');

    return 0;
}
