//header: functiile
#ifndef LABIRINT_H_INCLUDED
#define LABIRINT_H_INCLUDED

struct evidenta
{
    int V;
    int v;
};

struct coordonate
{
    int x;//x=1;//linii
    int y;//y=2;//coloane
};

void print(int m, int n, char a[100][100]);
void dead();
void win();
void meniu();
void reguli();

#endif // LABIRINT_H_INCLUDED
