# Labirint_C_Project

## Description

În cadrul acestui proiect s-a încercat realizarea în limbaj c a unui joc simplu ce folosește același principiu ca acela al unui labirint în care jucătorul trebuie să găsească drumul potrivit. Scopul principal îl reprezintă colectarea celor 10  steluțe „*” din cadrul jocului și evitarea capcanelor „H” care odată colectate vor duce la pierderea uneia dintr-un total de 3 vieți. Interfața jocului este una extrem de simpla fiind reprezentata de caractere ce pot fi ușor regăsite pe tastatura oricărui calculator, pereții sunt reprezentați de majuscule ale literei „X” iar coridoarele sunt caractere „_’’ , aspectul labirintului poate fi modificat cu ușurința din fișierul „date.in” dar programul va ignora instant oricare alt caracter, doar caracterele „*’’,”H”,”X” vor fi luate în considerare, celelalte urmând a fi înlocuite automat cu „_” odată cu mișcarea caracterului „P” ce reprezinta poziția jucătorului.


